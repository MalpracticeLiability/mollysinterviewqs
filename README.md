# Practice Interview/CS101 Problems #

### Find the remainder: Given a numerator and a denominator, find the remainder

Example: (User input denoted by ~)

```
Enter a numerator: ~7
Enter a denominator: ~4
	
The remainder of 7/4 is 3.
```

Hints:

* Remember what math ops you used for FizzBuzz.

### Find Max Value: Given a list of numbers inputted by the user, find the max value *without* using a .max() function.
Example: (User input denoted by ~)

```
Enter a number: ~5
	Do you have more? (y/n): y
Enter a number: ~4
	Do you have more? (y/n): y
Enter a number: ~10
	Do you have more? (y/n): y
Enter a number: ~1
	Do you have more? (y/n): y
Enter a number: ~8
	Do you have more? (y/n): n
	
Maximum value: 10
```

Hints:

*	We've discussed a method of finding a max in one of your previous REPLs. The code is likely still in my personal REPL.

Bonus:

* Do the same problem but for the minimum instead


### Find Duplicates: Given a list of numbers, return all duplicates
Example: (User input denoted by ~)

```
Enter a number: ~5
	Do you have more? (y/n): y
Enter a number: ~8
	Do you have more? (y/n): y
Enter a number: ~10
	Do you have more? (y/n): y
Enter a number: ~5
	Do you have more? (y/n): y
Enter a number: ~7
	Do you have more? (y/n): y
Enter a number: ~8
	Do you have more? (y/n): n
	
Duplicates: 5, 8
```

Hints:

* You can take the same approach from your max/min problem and adapt it to find duplicates


### Check Permutation: Given two strings, write a method to decide if one is a permutation of the other

Hints:

* What characters would two strings have in common if they're permutations of the others? How can you adapt Problem #1 to solve this problem?


### URLify: Write a method to replace all spaces of a string with '%20"
Example: Mollys Ada Prep => Mollys%20Ada%20Prep

Hints:

* Find methods to iterate through each character within a string
* How would you insert characters? Decide if you want to do in-place or create a new string entirely and append as you go along


### Reverse String: Given a string, print out its reverse.
Example: "Molly's Ada Prep" => "preP adA s'ylloM"

Hints:

* Find methods to iterate through each character within a string
* Figure out if you want to do an in-place reversal (not recommended) or save the correct output to another string before printing it (highly recommended)


### Palindrome Permutation: Given a string, check if it is a permutation of a palindrome

Hints:

* What does a palindrome entail? 
* If a string is a permutation of a palindrome, does the order of the characters matter?
* Pay special attention to the number count of characters in a permutation of a palindrome. This is a math question more than an English question.


### Is Unique: Implement an algorithm to determine is a string has all unique characters.

Hints:

* Find methods to iterate through each character within a string
* Find data structure to keep track of characters already found


### Trim Whitespace: Implement a program that trims leading and trailing whitespace from a string. Do *not* remove whitespaces in the middle of the string.
Example: "             Molly's Ada Prep   " => "Molly's Ada Prep"
Be careful not to turn the above string into "Molly'sAdaPrep".

Hints:

* Find methods to iterate through each character within a string
* Figure out how to differentiate between leading/trailing whitespace and whitespace in the middle of the string
* Edge cases will be the killer here. Most of your problems will likely be figuring out if a whitespace is leading/trailing or in the middle of the string



